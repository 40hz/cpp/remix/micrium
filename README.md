# Micrium uC/OS-III for Remix

This is a PlatformIO package which provides Micrium to be used in conjunction
with the [Remix](https://gitlab.com/40hz/cpp/remix) platform and library.

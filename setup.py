Import("env")
Import("projenv")

from os.path import join, realpath

env.Append(CPPPATH=[
    realpath(join("src", "uC-OS3", "Source")),
    realpath(join("src", "uC-OS3", "Ports", "ARM-Cortex-M" ,"ARMv7-M", "GNU")),
    realpath(join("src", "uC-CPU")),
    realpath(join("src", "uC-CPU", "ARM-Cortex-M" ,"ARMv7-M", "GNU")),
    realpath(join("src", "uC-LIB")),
    realpath(join("src", "uC-LIB/Cfg")),
    projenv["PROJECT_SRC_DIR"]])
env.Replace(SRC_FILTER=[
    "+<uC-CPU/cpu_core.c>",
    "+<uC-CPU/ARM-Cortex-M/ARMv7-M/*.c>",
    "+<uC-CPU/ARM-Cortex-M/ARMv7-M/GNU/*.s>",
    "+<uC-LIB/*.c>",
    "+<uC-OS3/Source>",
    "+<uC-OS3/Ports/ARM-Cortex-M/ARMv7-M/*.c>",
    "+<uC-OS3/Ports/ARM-Cortex-M/ARMv7-M/GNU/*.S>"])

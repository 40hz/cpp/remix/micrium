#pragma once

#include "irqhandler.h"
#include <optional>
#include <os.h>
#include <remix.h>
#include <remix/buffer.h>
#include <span>
#include <string.h>
#include <string_view>

namespace remix {

template<typename Serial>
struct HalfDuplex : public IrqHandler {
    Serial& serial;

    Buffer<uint8_t> input {};
    Buffer<const uint8_t> output {};
    std::optional<uint8_t> inputDelimiter {};

    HalfDuplex(Serial& s)
        : serial(s)
    {
    }

    auto write(const char* string) -> void
    {
        write(std::span { reinterpret_cast<const uint8_t*>(string), strlen(string) });
    }

    auto write(std::string_view string) -> void
    {
        write(std::span { reinterpret_cast<const uint8_t*>(string.begin()), string.size() });
    }

    auto write(std::span<const uint8_t> data) -> void
    {
        output.init(data);
        transactionInit();
        serial.enableTxIrq();
        transactionAwait();
    }

    auto read(std::span<char> data, std::optional<uint8_t> delimiter = {}) -> std::span<char>
    {
        auto r = read(std::span(reinterpret_cast<uint8_t*>(data.data()), data.size()), delimiter);
        return std::span(data.begin(), r.size());
    }

    auto read(std::span<uint8_t> data, std::optional<uint8_t> delimiter = {}) -> std::span<uint8_t>
    {
        input.init(data);
        inputDelimiter = delimiter;
        transactionInit();
        serial.enableRxIrq();
        transactionAwait();
        return { data.begin(), input.current };
    }

    auto send(uint8_t data) -> void { write({ &data, 1 }); }

    auto receive() -> uint8_t
    {
        uint8_t data[1];
        read({ &data[0], 1 });
        return data[0];
    }

    auto sendHandler() -> void
    {
        if (!output.atEnd()) {
            serial.tx(*output.current++);
        }
        if (output.atEnd()) {
            transactionDone();
            serial.disableTxIrq();
        }
    }

    auto receiveHandler() -> void
    {
        bool readDelimiter { false };
        if (!input.atEnd()) {
            uint8_t rx = serial.rx();
            if (inputDelimiter == rx) {
                readDelimiter = true;
            } else {
                *input.current++ = serial.rx();
            }
        }
        if (readDelimiter || input.atEnd()) {
            transactionDone();
            serial.disableRxIrq();
        }
    }
};

}

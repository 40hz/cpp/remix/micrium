#pragma once

#include <os.h>

namespace remix {

struct IrqHandler {
    OS_TCB* caller {};

    auto transactionDone() -> void
    {
        OS_ERR err;
        OSTaskSemPost(caller, OS_OPT_POST_NONE, &err);
        while (err != OS_ERR_NONE)
            ;
    }

    auto transactionInit() -> void { caller = OSTCBCurPtr; }

    auto transactionAwait() -> void
    {
        OS_ERR err;
        OSTaskSemPend(0, OS_OPT_PEND_BLOCKING, NULL, &err);
        while (err != OS_ERR_NONE)
            ;
    }

};

}
